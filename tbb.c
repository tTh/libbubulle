/*
 * 	this is rudimentary test for the bubulles manager
 *
 * 	contact: <tth> on IRC freenode#tetalab
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  "bubulles.h"
#include  "edges.h"

/* --------------------------------------------------------------------- */
int essai_des_edges_A(int k)
{
EdgeList	* list;
int		foo, idx;
int		e0, e1;

fprintf(stderr, "============== %s %7d ===========\n", __func__, k);

list = alloc_edgelist("oups?", k, 0);
fprintf(stderr, "    alloc edge list -> %p\n", list);

print_edgelist_desc(list, 0);

foo = push_an_edge(list, 13, 37);
fprintf(stderr, "    push edge -> %d\n", foo);
foo = push_an_edge(list, 24, 36);
fprintf(stderr, "    push edge -> %d\n", foo);

foo = print_the_edges(stdout, list, 0);

for (idx=0; idx<k; idx++) {
	e0 = idx*7;	e1 = 5-idx;
	foo = push_an_edge(list, e0, e1);
	if (foo) {
		fprintf(stderr, "push %d (%d, %d) -> %d\n", idx, e0, e1, foo);
		break;
		}
	}

foo = print_the_edges(stdout, list, 0);		puts("");

foo = free_edgelist(list, 0);
fprintf(stderr, "    free list -> %d\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *	ici on va faire des essais sur la gestion
 *	de la dé-duplication des aretes.
 */
int essai_des_edges_B(int k)
{
EdgeList	* list;
int		foo, idx;
int		e0, e1;

fprintf(stderr, "============== %s %7d ===========\n", __func__, k);

list = alloc_edgelist("gloubigoulba", k, 0);
if (NULL == list) {
	fprintf(stderr, "%s: epic fail\n", __func__);
	abort();
	}

print_edgelist_desc(list, 0);

for (idx=0; idx<k; idx++) {
	e0 = rand() % 666;
	e1 = rand() % 666;
	foo = push_a_missing_edge(list, e0, e1);
	}

print_edgelist_desc(list, 0);

foo = print_the_edges(stdout, list, 0);		puts("");

foo = free_edgelist(list, 0);
fprintf(stderr, "    %s: free list -> %d\n", __func__, foo);

return -1;
}


/* --------------------------------------------------------------------- */

void test_alloc_free(int nbelm)
{
BBList		*bublist;
int		foo;

bublist = alloc_bubulles("plop !", nbelm, 0);
if (NULL==bublist) {
	fprintf(stderr, "err in %s, aborting...\n", __func__);
	abort();
	}
print_bublist_desc(bublist, 1);

foo = free_bubulles(bublist, 0);
fprintf(stderr, "free bubulles -> %d\n", foo);

}
/* --------------------------------------------------------------------- */
int test_push_pop(int nbelm)
{
BBList		*bublist;
int		foo, idx;
Bubulle		bubulle;

bublist = alloc_bubulles("push & pop", nbelm, 0);

for (idx=0; idx<nbelm; idx++) {
	bubulle.p.x = (double)idx;
	if (idx & 0xf0) 
		bubulle.p.y = bubulle.p.z = 0.3;
	else
		bubulle.p.y = bubulle.p.z = 0.8;
	foo = push_bubulle(bublist, &bubulle);
	}

/* WTF ? where is the 'pop' test ? */

foo = bubulles_to_data("dummy-file", NULL, bublist, 0);	
fprintf(stderr, "ecriture bubulles -> %d\n", foo);

foo = free_bubulles(bublist, 0);
fprintf(stderr, "free bubulles -> %d\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */
int test_cleanfill_my_bublist(int krkrkr)
{
BBList		*bublist;
int		foo;

bublist = alloc_bubulles("cleanfill", 567890, 0);
foo = cleanfill_my_bublist(bublist, 0);
fprintf(stderr, "clean & fill -> %d\n", foo);
free_bubulles(bublist, 0);

return 0;
}
/* --------------------------------------------------------------------- */
int test_peek_poke(int nbre)
{
BBList		*bublist;
int		foo, in, out;
Bubulle		bubulle;

bublist = alloc_bubulles("peek & poke", nbre, 0);
print_bublist_desc(bublist, 0);

in = nbre /  2;
out = nbre + 42;

foo = peek_bubulle(bublist, &bubulle, in);
fprintf(stderr, "peek 1     -> %7d\n", foo);

foo = niceprint_bubulle(&bubulle, 0);
if (foo) fprintf(stderr, "niceprint  -> %d\n", foo);

foo = peek_bubulle(bublist, &bubulle, out);
fprintf(stderr, "peek 2     -> %7d\n", foo);

bubulle.p.y = 3.14159;
foo = poke_bubulle(bublist, &bubulle, in);
fprintf(stderr, "poke 2     -> %7d\n", foo);

foo = peek_bubulle(bublist, &bubulle, in);
fprintf(stderr, "peek 3     -> %7d\n", foo);

foo = niceprint_bubulle(&bubulle, 0);
if (foo) fprintf(stderr, "niceprint  -> %d\n", foo);

free_bubulles(bublist, 0);

return 0;
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo;

fprintf(stderr, "***\n*** Bubulles Testing  %s %s\n***\n",
				__DATE__, __TIME__);

bubulles_version(0);

srand(getpid());		/*	INIT RANDOM	*/

#if (0)
test_alloc_free(5);
test_push_pop(20000);
test_cleanfill_my_bublist(999);
foo = test_peek_poke(5000);
fprintf(stderr, "test peek/poke -> %d\n", foo);
#endif


foo = essai_des_edges_A(25);
fprintf(stderr, "test A des edges -> %d\n", foo);
foo = essai_des_edges_B(5000);
fprintf(stderr, "test B des edges -> %d\n", foo);

return 0;
}
