/*
 *		edges.h
 *	a part of libbubulle from tTh
 *
 *		https://git.tetalab.org/tTh/libbubulle
 */

/* --------------------------------------------------------------------- */

typedef struct {
	int		A, B;
	short		burz;			/* for what usage ??? */
	} AnEdge;

typedef struct {
	unsigned long	magic;
	char		name[SZ_BUBULLE_TEXT+1];
	int		size;			/* max number of edges	*/
	int		fidx;			/* next free slot	*/
	AnEdge		*edges;
	unsigned long	flags;
	} EdgeList;

/* --------------------------------------------------------------------- */

EdgeList	* alloc_edgelist(char *name, int sz, int flags);
int 		free_edgelist(EdgeList *list, int k);
int 		push_an_edge(EdgeList *list, int p0, int p1);
int 		push_a_missing_edge(EdgeList *list, int p0, int p1);

// int 		is_edge_in_list(EdgeList *list, int p0, int p1);

int  		print_edgelist_desc(EdgeList *list, int k);
int 		edges_to_data(char *fname, EdgeList *list, int k);
int  		print_the_edges(FILE *file, EdgeList *list, int k);

/* --------------------------------------------------------------------- */
