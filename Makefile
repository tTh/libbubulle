
#############################################
#
#	making the bubulles library
#
#	for compiling on FreeBSD :
#	   $ gmake CC=clang tbb
#
#############################################

CC = gcc

OPT = -Wall -g -DDEBUG_LEVEL=0 -DMUST_ABORT=0

libbubulles.a:	bubulles.o edges.o
	ar r $@ $?

bubulles.o:	bubulles.c bubulles.h Makefile
	$(CC) $(OPT) -c $< 

edges.o:	edges.c bubulles.h edges.h Makefile
	$(CC) $(OPT) -c $< 

# ------------------------------------------------
# --- build some tests and tools

tbb:		tbb.c bubulles.h libbubulles.a Makefile
	$(CC) $(OPT)  $< libbubulles.a -o tbb

#############################################
