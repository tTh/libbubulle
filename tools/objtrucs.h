/*
 *		EXPERIMENTAL CODE !
 *
 *	see  'rdwredges.c'       for source code
 *        &  'export_evblob.c'   for a usecase.
 */

#define  EVBLOB_MAGIC    (65872139)

typedef struct {
	unsigned long   	magic;
	BBList			*Blist;
	EdgeList		*Elist;
	int			status;
	} EdgesAndVertices;

/* in importobj.c */
int try_to_read_an_OBJ_file(char *fname, char *oname, int outstyle);

/* in rdwredges.c */
int x_write_vertedges (char *filename, BBList *bblist, EdgeList *edges);
int x_load_vertedges  (char *filename, EdgesAndVertices *eav);
