# Importer des fichiers .OBJ

* https://en.wikipedia.org/wiki/Wavefront_.obj_file
* http://fegemo.github.io/cefet-cg/attachments/obj-spec.pdf
* https://people.sc.fsu.edu/~jburkardt/data/obj/obj.html

```
v -1.177934647 -6.833468914 -73.19773865
vn -0.1279897094 -0.4501263499 -0.8837448359
v -0.8008174896 -6.425453663 -73.32041931
vn -0.05844723806 -0.09480132163 -0.993778944
```

## read_obj

Première étape : en lisant les vertices, nous saurons positionner
nos bubulles. Ensuite, en explorant les faces, nous pouvons
en déduire les arètes (aka: edges).

Et à partir de cette analyse, nous allons générer un fichier
binaire contenant les points xyx, et les arêtes pointA-pointB.
Le format de ce fichier doit être considéré *opaque*.
il sera généré dans le $PWD en utilisant le basename du fichier .OBJ mais
avec l'extension `.evblob`.

Ce n'est qu'une première étape, d'autres formats de sortie
pourront être implémentés.

Attention, mon parser EXIGE des fichiers Unix bien conformés :
c'est-à-dire que la dernière ligne du `.obj` DOIT être terminée
par un newline !

## export_evblob & bla.awk

Deuxième étape : À partir du fichier .evblob généré à l'étape
précédente, nous allons créer (entre autres options ?)
un fichier utilisable par Povray. La première option a été
la génération des arêtes, nous aurons donc sur `stdout`
six nombres réels: xyz(pointA) et xyz(pointB) :

```
25.2275466 19.5029792 -25.1227169   35.6406135 19.5029792 0.0166420
35.6406135 19.5029792 0.0166420   0.0881849 19.5029792 -35.5357818
0.0881849 19.5029792 -35.5357818   25.2275466 19.5029792 -25.1227169
```

Et là, c'est trop facile pour la suite, Unix sait le faire depuis
quarante ans.
Un petit [script Awk](./edges2cylinders.awk) fera l'affaire,
en toute simplicité.

Bien entendu, la même chose pourrait être faire pour les vertices,
ce qui nous remet dans la droite ligne orthodoxe des bubulles.

## TODO LIST

* Songer à un système d'auto-scaler et de recentrage
* Comment générer un `.obj` à partir d'une image flottante ?
* Gérer les arêtes de longueur nulle (degenerated cylinder)
* Que faire des vertices qui ne sont pas utilisées par des faces ?
* Plonger un peu dans la [technique](./technique.md).

## rendu final

Quatrième étape : aller vivre à la campagne ?


