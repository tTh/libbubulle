# Technique

Les détails **gores**...

## Le parser

Je pense qu'il y a un bug sournois caché dedans

## L'exporteur

Conversion d'un `.evblob` en données tabulées exploitables par
des scripts `awk`.

## Tests

Pour le moment rien, à part ce fichier .OBJ vérolé :

```
o minimal

v 0 0 0
v 1 0 0
v 0 2 0
v 0 0 3

f 0 1 2
f 2 3 4
f 4 2 1
f 0 3 4
```

Ça va, vous avez capté le souci ?

## Conclusion

Rien ,'est simple, tout se complique...

