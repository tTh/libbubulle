#!/bin/bash

set -e

make read_obj
make export_evblob

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
figlet 'a real WTF' | sed 's/^/XXX  /'
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

./read_obj -v minimal.obj

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

./export_evblob -t edges minimal.evblob		|
	awk '{ printf "%4d  |  %.0f  %.0f  %.0f    %.0f  %.0f  %.0f\n", \
			NR, $1, $2, $3,	$4, $5, $6 }'
echo

./export_evblob -t vertices minimal.evblob


