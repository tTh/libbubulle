/*
     tentatives de lecture des OBJ
*/

#include	<stdio.h>
#include	<stdlib.h>
#include	<libgen.h>		// for basename(3)
#include        <unistd.h>

#include	"../bubulles.h"
#include	"../edges.h"

#include	"objtrucs.h"

int	verbosity = 0;

/* --------------------------------------------------------------------- */
void help(void)
{
printf("### READ_OBJ       0X%X TALKING\n", getpid());
puts("usage:");
puts("\t-h\t\tthis help (use -v -h for more");
puts("\t-o fname\tfix the output filename");
puts("\t-v\t\tincrease verbosity");

exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo, opt;
char		*ofname = NULL;

fprintf(stderr, "### READ_OBJ       compiled %s at %s\n", __DATE__, __TIME__);

if (1 == argc) {
	bubulles_version(1);
	exit(0);
	}

while ((opt = getopt(argc, argv, "ho:v")) != -1) {
	switch(opt) {
		case 'h':
			help();			break;
		case 'o':
			ofname = optarg;
			break;
		case 'v':
			verbosity++;		break;
		default:
			exit(1);
			break;
		}
	}

if (optind < argc) {
	if (verbosity) fprintf(stderr, "    arg[%d] = %s\n",
					optind, argv[optind]);
	foo = try_to_read_an_OBJ_file(argv[optind], ofname, 0);
	if (foo) {
		fprintf(stderr, "Error number %d on '%s'\n",
						foo, argv[optind]);
		exit(1);
		}
	}
else	{
	fprintf(stderr, "%s need a input filename\n", argv[0]);
	exit(1);
	}

return 0;
}
