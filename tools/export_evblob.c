/*
 *		EdgesAndVertices

	https://git.tetalab.org/tTh/libbubulle/src/branch/master/tools/

 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <unistd.h>

#include  "../bubulles.h"
#include  "../edges.h"

#include  "objtrucs.h"

int		verbosity;

/* --------------------------------------------------------------------- */
/*			EXPERIMENTAL GRUIK-CODE !!!			 */
int printf_the_boudingbox(EdgesAndVertices *eav)
{
int		idx;
double		x, y, z;
double		minX, minY, minZ;
double		maxX, maxY, maxZ;

minX = minY = minZ =  1e35;
maxX = maxY = maxZ = -1e35;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, eav);
#endif

fprintf(stderr, "====== %s is not fully implemented ======\n", __func__);

for (idx=0; idx<eav->Blist->fidx; idx++) {
	x = eav->Blist->bbs[idx].p.x;
	y = eav->Blist->bbs[idx].p.y;
	z = eav->Blist->bbs[idx].p.z;

	if	(x < minX)		minX = x;
	else if (x > maxX)		maxX = x;
	if	(y < minY)		minY = y;
	else if (y > maxY)		maxY = y;
	if	(z < minZ)		minZ = z;
	else if (z > maxZ)		maxZ = z;

	}

printf(		"%.9f %.9f %.9f   %.9f %.9f %.9f\n",
		minX, minY, minZ, maxX, maxY, maxZ);

return -1;
}
/* --------------------------------------------------------------------- */
/*			EXPERIMENTAL GRUIK-CODE !!!			 */
int printf_the_vertices(EdgesAndVertices *eav)
{
int		idx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, eav);
#endif

if (verbosity) fprintf(stderr, "fprinting %d vertices\n", eav->Blist->fidx);

for (idx=0; idx<eav->Blist->fidx; idx++) {
	printf("%.9f %.9f %.9f\n",
		eav->Blist->bbs[idx].p.x,
		eav->Blist->bbs[idx].p.y,
		eav->Blist->bbs[idx].p.z	);
	}

return 0;
}
/* --------------------------------------------------------------------- */
/*			EXPERIMENTAL GRUIK-CODE !!!			 */
int printf_the_edges(EdgesAndVertices *eav)
{
int		idx, a, b, vmax;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, eav);
#endif

if (verbosity) fprintf(stderr, "    fprinting %d edges\n", eav->Elist->fidx);

vmax = eav->Blist->fidx;
fprintf(stderr, "    %s: eav->Blist->fidx = %d\n", __func__, vmax);

/*
 *	OK we have (maybe) all the data in da place
 *	and we can spit all the edges to stdout
 */
for (idx=0; idx<eav->Elist->fidx; idx++) {

	a = eav->Elist->edges[idx].A;
	b = eav->Elist->edges[idx].B;

	/* this is a Molly-Guard */
	if ( (a<0) || (b<0) || (a>vmax) || (b>vmax) ) {
		fprintf(stderr, "ERROR: vmax=%d a=%d b=%d\n", vmax, a, b);
		continue;
		}

	printf("%.9f %.9f %.9f   %.9f %.9f %.9f\n",
		eav->Blist->bbs[a].p.x,
		eav->Blist->bbs[a].p.y,
		eav->Blist->bbs[a].p.z,
		eav->Blist->bbs[b].p.x,
		eav->Blist->bbs[b].p.y,
		eav->Blist->bbs[b].p.z	);
	}

return 0;		/* allway success ? */
}
/* --------------------------------------------------------------------- */
/*			EXPERIMENTAL GRUIK-CODE !!!			 */
int load_and_printf_evblob(char *filename, char *outmode)
{
EdgesAndVertices	eav;
int			foo, mode;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s'  %s )\n", __func__, filename, outmode);
#endif

memset(&eav, 0, sizeof(EdgesAndVertices));
foo = x_load_vertedges(filename, &eav);
if (foo) {
	fprintf(stderr, "    load vertice & edges blob -> %d\n", foo);
	return foo;
	}

if (verbosity > 1) {
	fprintf(stderr, "vertices at   %p\n", eav.Blist);
	fprintf(stderr, "edges at      %p\n", eav.Elist);
	fprintf(stderr, "status is     %d\n", eav.status);
	fprintf(stderr, "got %d vertices and %d edges\n",
				eav.Blist->fidx,
				eav.Elist->fidx);
	}

mode = 666;
if      (! strcasecmp("edges",    outmode)) { mode = 0; }
else if (! strcasecmp("vertices", outmode)) { mode = 1; }
else if (! strcasecmp("bbox",     outmode)) { mode = 2; }

switch (mode) {
	case 0:
		foo = printf_the_edges(&eav);			break;
	case 1:
		foo = printf_the_vertices(&eav);		break;
	case 2:
		foo = printf_the_boudingbox(&eav);		break;

	default:
		fprintf(stderr, "%s: no way for '%s'\n", __func__, outmode);
		exit(1);
		break;
	}

if (foo) {
	fprintf(stderr, "%s: action '%s' -> %d\n", __func__, outmode, foo);
	exit(1);
	}

/* deallocate used memory for keep pinpin happy */
free_bubulles(eav.Blist, 1);
free_edgelist(eav.Elist, 1);

return 0;
}
/* --------------------------------------------------------------------- */
void help(void)
{

puts("usage:\n  $ export_evblob [opts] fichier.evblob");
puts("\t-h\tsome short help...");
puts("\t-t\tone of [edges vertices bbox]");
puts("\t-v\tbe more verbose");

exit(0);
}
/* --------------------------------------------------------------------- */
/*			EXPERIMENTAL GRUIK-CODE !!!			 */

int main(int argc, char *argv[])
{
int			opt, foo;
char			*outmode = "vertices";

fprintf(stderr, "### Export EVblob   (%s %s)\n", __DATE__, __TIME__);

verbosity = 0;

while ((opt = getopt(argc, argv, "ht:v")) != -1) {
	switch(opt) {
		case 'h':
			help();			break;
		case 'v':
			verbosity++;		break;
		case 't':
			// fprintf(stderr, "type -> '%s'\n", optarg);
			outmode = optarg;
			break;
		default:
			fprintf(stderr, "'%c' EBADOPT\n", opt);
			exit(1);
			break;
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "optind=%d argc=%d\n", optind, argc);
#endif

if (optind < argc) {
	// fprintf(stderr, "ARG = %s\n", argv[optind]);
	foo = load_and_printf_evblob(argv[optind], outmode);
	if (foo) {
		fprintf(stderr, "Error number %d on '%s'\n", foo, argv[optind]);
		exit(1);
		}
	}
else	{
	fprintf(stderr, "%s need a input filename\n", argv[0]);
	exit(1);
	}

return 0;
}
/* --------------------------------------------------------------------- */

