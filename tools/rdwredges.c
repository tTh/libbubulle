/*
	          LIBBUBULLES IMPORT_OBJ
	some functions for importing bubulles from dot-OBJ files.

	https://git.tetalab.org/tTh/libbubulle/src/branch/master/tools/
	http://fegemo.github.io/cefet-cg/attachments/obj-spec.pdf

*/

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <unistd.h>

#include  "../bubulles.h"
#include  "../edges.h"

#include  "objtrucs.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
static void dumper(void *ptr, int count)
{
int		idx;
unsigned char	*cptr = (unsigned char *)ptr;

for (idx=0; idx<count; idx++) {
	fprintf(stderr, "%02x ", cptr[idx]);
	}
fprintf(stderr, "    ?\n");
for (idx=0; idx<count; idx++) {
	fprintf(stderr, " %c ", isprint(cptr[idx]) ? cptr[idx] : 'X' );
	}
fprintf(stderr, "    ?\n");
}
/* --------------------------------------------------------------------- */
/*			EXPERIMENTAL GRUIK-CODE !!!			 */
int x_write_vertedges(char *filename, BBList *bblist, EdgeList *edges)
{
FILE		*fp;
int		idx, edg[2];
double		coo[3];
int		nbre;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s'  %p  %p )\n", __func__, filename,
						bblist, edges);
#endif

if (NULL==(fp=fopen(filename, "w"))) {
	perror(filename);
	return -4;
	}

// print_bublist_desc(bblist, 0);
fwrite("VERTICES", 8, 1, fp);
nbre = bblist->fidx;
fwrite(&nbre, sizeof(int), 1, fp);
for (idx=0; idx<bblist->fidx; idx++) {
	// fprintf(stderr, "vertice %d\n", idx);
	coo[0] = bblist->bbs[idx].p.x;
	coo[1] = bblist->bbs[idx].p.y;
	coo[2] = bblist->bbs[idx].p.z;
	fwrite(coo, sizeof(coo), 1, fp);
	}

// print_edgelist_desc(edges, 0);
fwrite("EDGES   ", 8, 1, fp);
nbre = edges->fidx;
fwrite(&nbre, sizeof(int), 1, fp);
for (idx=0; idx<edges->fidx; idx++) {
	edg[0] = edges->edges[idx].A;
	edg[1] = edges->edges[idx].B;
	fwrite(edg, sizeof(edg), 1, fp);
	}
fclose(fp);

return 0;
}
/* --------------------------------------------------------------------- */
/*			EXPERIMENTAL GRUIK-CODE !!!			 */
int x_load_vertedges(char *filename, EdgesAndVertices *eav)
{
BBList		*blst;
EdgeList	*elst;
FILE		*fp;
char		marker[8];
int		nbre, foo, idx, edg[2];
double		coo[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p )\n", __func__, filename, eav);
#endif

if (NULL==(fp=fopen(filename, "r"))) {
	perror(filename);
	return -4;
	}

/* + + + + + + + + + + + */
foo = fread(marker, 8, 1, fp);
if (1 != foo) {
	fprintf(stderr, "    %s of '%s': short read %d\n", __func__, filename, foo);
	return -1;
	}
if (memcmp("VERTICES", marker, 8)) {
	fprintf(stderr, "    %s is not an evblob file.\n", filename);
	dumper(marker, 8);
	return -4;
	}

/* now, get the numbers of vertices */
foo = fread(&nbre, sizeof(int), 1, fp);
if (1 != foo) {
	fprintf(stderr, "    %s of '%s': short read %d\n", __func__, filename, foo);
	return -1;
	}
if (verbosity) fprintf(stderr, "    %d vertices to be loaded\n", nbre);

/* allocate memory */
blst = alloc_bubulles(filename, nbre, 0);
if (NULL==blst) {
        fprintf(stderr, "in %s, no mem, aborting...\n", __func__);
        abort();
        }
if (verbosity > 1)  print_bublist_desc(blst, 0);

/* load all that XYZ points */
for (idx=0; idx<nbre; idx++) {
	if (3 != fread(coo, sizeof(double), 3, fp)) {
		fprintf(stderr, "%s err reading vertice #%d\n", __func__, idx);
		exit(1);
		}
	blst->bbs[idx].p.x = coo[0];
	blst->bbs[idx].p.y = coo[1];
	blst->bbs[idx].p.z = coo[2];
	blst->fidx++;
	}

/* + + + + + + + + + + + */
foo = fread(marker, 8, 1, fp);
if (1 != foo) {
	fprintf(stderr, "    %s of '%s': short read %d\n", __func__, filename, foo);
	return -1;
	}
if (memcmp("EDGES   ", marker, 8)) {
	fprintf(stderr, "    %s is not an evblob file.\n", filename);
	dumper(marker, 8);
	return -4;
	}
/* now, get the numbers of eges */
foo = fread(&nbre, sizeof(int), 1, fp);
if (1 != foo) {
	fprintf(stderr, "    %s of '%s': short read %d\n", __func__, filename, foo);
	return -1;
	}
if (verbosity) fprintf(stderr, "    %d edges to be loaded\n", nbre);

/* allocate memory for edges list */
elst = alloc_edgelist("krkrkr", nbre, 0);
if (NULL==elst) {
        fprintf(stderr, "no mem for edges in %s, aborting...\n", __func__);
        abort();	/* be violent before OOMK */
        }

/* read (and check ?) the edges */
for (idx=0; idx<nbre; idx++) {
	if (2 != fread(edg, sizeof(int), 2, fp)) {
		fprintf(stderr, "%s: err reading edge #%d\n", __func__, idx);
		return -6;
		}
	elst->edges[idx].A = edg[0];
	elst->edges[idx].B = edg[1];
	elst->fidx++;
	}

/* so, now I think we have got all the data */
eav->Blist  = blst;
eav->Elist  = elst;
eav->status = 0x51;

fclose(fp);

return 0;
}
/* --------------------------------------------------------------------- */

