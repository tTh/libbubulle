/*
	bubulles.h - dirty software from tTh
*/

/* --------------------------------------------------------------------- */

#define LIBBB_VERSION		64

#define SZ_BUBULLE_TEXT		81		/* arbitrary value */

/* a 3d space coordinate */
typedef struct {
	double		x, y, z;
	unsigned long	reserved;
	} XYZ;

/* colors and transparency */
typedef struct {
	float		r, g, b, a;
	unsigned long	reserved;
	} RGBA;	

/*
           this is our main entity : the bubulle.
   'bubulle' is a slang french word for 'friendly bubble'.
*/
typedef struct {
	XYZ		p;			/* position */
	double  	d;			/* diameter */
	int		gray;			/* used as an index */
	RGBA		col;
	long		ttl;			/* bubulles can be aged */
	double		kvalue;			/* wtf ? */
	} Bubulle;

/*
   this is the bubulles list descriptor.
*/
typedef struct {
	char		name[SZ_BUBULLE_TEXT+1];
	int		size;			/* max number of bubulles */
	int		fidx;			/* next free slot */
	XYZ		position;		/* global position */
	unsigned long	flags;
	Bubulle		*bbs;			/* ptr to a movable array */
	} BBList;


BBList * alloc_bubulles(char *name, int nbre, int k);
int	free_bubulles(BBList *bbl, int k);
int	print_bublist_desc(BBList *bbl, int k);
int	push_bubulle(BBList *where, Bubulle *what);
int	poke_bubulle(BBList *where, Bubulle *what, int idx);
int	peek_bubulle(BBList *from, Bubulle *to, int idx);

int	cleanfill_my_bublist(BBList *what, int k);
int	bubulles_version(int k);
void	bubulles_sizeof(int k);

Bubulle * bubulle_getaddr(BBList *where, int idx);

/*
 *	export functions for everything
 */
/*	TO BE DONE XXX			*/

/*
 *		export Awk machinable.
 * flags :
 *	0x0001		print diameter
 *	0x0002		print graylevel
 *	0x0004		print RGB values
 */
int fprint_bubulles(FILE *fp, char *title, BBList *bbl, int flags);

/* only human readable */
int niceprint_bubulle(Bubulle *what, int unused);

/* this is just a wtf function * see tbb.c */
int bubulles_to_data(char *fname, char *title, BBList *bbl, int flags);

/* --------------------------------------------------------------------- */
/* sometime we want to look at the bounding box */
typedef struct {
	double		minX, minY, minZ;
	double		maxX, maxY, maxZ;
	int		flags;
	} BBox;

int bounding_box(Bubulle *boubs, int nbre, BBox *bbox);
int print_bbox(BBox *bbox, int k);

/* --------------------------------------------------------------------- */
